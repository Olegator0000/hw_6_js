// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.


// Опишіть своїми словами, що таке метод об'єкту - метод обʼєкту це функція яка належить обʼєкту, this це метод обʼєкту який вказує саме на цей обʼєкт
// Який тип даних може мати значення властивості об'єкта? - багато типів даних такі як  функції, рядки, числа, логічні значення, обʼєкти які входять в інші обʼєкти.
// Об'єкт це посилальний тип даних. Що означає це поняття? ми коли створюємо обʼєкт ми створюємо посилання на цей обʼєкт, наприклад:    const man = { name : 'oleh'}


// function createNewUser() {
// let name = prompt('введіть своє ім"я')
// let surname = prompt('введіть прізвище')
// let birthday = prompt('Введіть дату народження у форматі dd.mm.yyyy')
//
// let newUser = {
//     name: name,
//     surname: surname,
//     birthday: birthday,
//
//     getAge: function () {
//         let today = new Date();
//         let birthDate = new Date(this.birthday);
//         let age = today.getFullYear() - birthDate.getFullYear();
//         let month = today.getMonth() - birthDate.getMonth();
//         if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
//             age--;
//         }
//         return age;
//     },
//     getLogin: function () {
//         return (this.name[0] + this.surname).toLowerCase()
//         getPassword: function () {
//                 let firstLetter = this.name.charAt(0).toUpperCase();
//                 let password = firstLetter + this.surname.toLowerCase() + this.birthday.slice(6);
//                 return password;
//             }
//     }
//
//
// }
//
// let user = createNewUser();
//
// console.log('Age: ' + user.getAge());
// console.log('Password: ' + user.getPassword());





// Екранування - це захист деяких символів або рядків коду від неправильної інтерпретації.
// ми прописуємо:

// function message() {
//     alert('якесь повідомлення')
// }
//
// message()

// Hoisting - це коли ми підіймаємо змінну до початку блоку


function createNewUser() {
    let name = prompt('Введіть своє ім"я');
    let surname = prompt('Введіть прізвище');
    let birthday = prompt('Введіть дату народження у форматі dd.mm.yyyy');

    let newUser = {
        name: name,
        surname: surname,
        birthday: birthday,
        getAge: function() {
            let today = new Date();
            let birthDate = new Date(this.birthday);
            let age = today.getFullYear() - birthDate.getFullYear();
            let month = today.getMonth() - birthDate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },
        getPassword: function() {
            let firstLetter = this.name.charAt(0).toUpperCase();
            let password = firstLetter + this.surname.toLowerCase() + this.birthday.slice(6);
            return password;
        }
    };
    return newUser;
}

let user = createNewUser();
console.log(user);
console.log('Age: ' + user.getAge());
console.log('Password: ' + user.getPassword());

